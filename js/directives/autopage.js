
angular.module('autopage', []).directive('autoPage', function($document, $window) {
	return {
		restrict: 'A',
		link: function(scope, $el, attr) {
			var body,
				target;

			if (attr.hasOwnProperty('autoPageThis')) {
				target = $el[0];
				$el.bind('scroll', function() {
					if (target.scrollTop + target.offsetHeight >= target.scrollHeight) {
						scope.$apply(attr.autoPage);
					}
				});
			} else {
				body = $document[0].body;
				$document.bind('scroll', function() {
					if ($document.scrollTop() + $window.innerHeight >= body.scrollHeight) {
						scope.$apply(attr.autoPage);
					}
				});
			}
		}
	};
});