angular.module('autopageExample', [
	'ngRoute',
	'appRoutes',
	'ui.bootstrap',
	// controllers
	'MainCtrl',
	// directives
	'autopage'
]);