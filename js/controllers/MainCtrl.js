
// -----------------------------------------------------------------------------
//  Main Controller
//  Controller module for the site body.
// -----------------------------------------------------------------------------
angular.module('MainCtrl', [])
	.controller('MainController', function($scope, $rootScope, $http, $location) {
		
		var totalItems, pageSize, _list, init, getStubData;
		totalItems = 1000;
		pageSize = 10;

		$scope.listData = [];

		// the paging function
		$scope.getMore = function() {
			$scope.listData = $scope.listData.concat(_listData.splice(0, pageSize));
		}

		// show details overlay
		$scope.showDetails = function(id) {
			$scope.itemId = id;
		}

		// hide details overlay
		$scope.hideDetails = function() {
			$scope.itemId = null;
		}

		init = function() {
			_listData = getStubData(totalItems);
			$scope.getMore();
		};

		// just makes some stub data to use for testing
		getStubData = function(limit) {
			var listData = [];
			for (var i = 0; i < limit; i++) {
				listData.push({
					id: i,
					label: 'Item '+i,
					description: 'Eu do coba dizer tem cheguei your rumored falo. Voor itu and chegando kuwirt gila via treino lalalalala!'
				});
			}
			return listData;
		};

		init();
		
	});
